package com.demo.splitwise;

import com.demo.splitwise.dao.ExpenseDao;
import com.demo.splitwise.model.Expense;
import com.demo.splitwise.model.GroupBalance;
import com.demo.splitwise.service.ExpenseService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import javafx.util.Pair;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class DemoSplitWiseApplicationTests {
    org.slf4j.Logger logger = LoggerFactory.getLogger(DemoSplitWiseApplicationTests.class);

    @Autowired
    private ExpenseDao expenseDao;

    @Autowired
    private ExpenseService expenseService;

    @Test
    void contextLoads() {
    }

    @Test
    void testSimplifiedBal() {
        Expense exp = new Expense();
        exp.setGroupId(1);
        exp.setPaidBy(1);
        exp.setTotalBillAmount(400.0);
        exp.setType("fixed");
//        exp.setSplitsString("{\"1\":\"50\",\"2\":\"30\",\"3\":\"20\"}");

        expenseService.addExpense(exp);

        exp = new Expense();
        exp.setGroupId(1);
        exp.setPaidBy(2);
        exp.setTotalBillAmount(400.0);
        exp.setType("fixed");
//        exp.setSplitsString("{\"1\":\"50\",\"2\":\"30\",\"3\":\"20\"}");

        expenseService.addExpense(exp);


        GroupBalance bal = expenseService.getGroupBalance(1);

        List<Pair<Integer, Double>> payables = Lists.newArrayList();
        List<Pair<Integer, Double>> receivables = Lists.newArrayList();

        bal.getPayables().forEach((K, V) -> {

            payables.add(new Pair(K.getKey(), V));//from user
            receivables.add(new Pair(K.getValue(), V));//to user

        });

        logger.info("payables {}", payables.toString());
        logger.info("receivables {}", receivables.toString());

        Comparator<Pair<Integer, Double>> comparator = new Comparator<Pair<Integer, Double>>() {

//            @Override
//            public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
//                if (Math.abs(o1.getValue()- o2.getValue()) > .01) {
//                    return 1;
//                } else if (Math.abs(o1.getValue() - o2.getValue()) < .01) {
//                    return 0;
//                } else {
//                    return -1;
//                }
//            }

            @Override
            public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
                return -1 * o1.getValue().compareTo(o2.getValue());
            }
        };

        payables.sort(comparator);
        receivables.sort(comparator);

        logger.info("sorted payables {}", payables);
        logger.info("sorted receivables {}", receivables);

        logger.info("sorting done");

        int lenPayables = payables.size();
        int lenReceivables = receivables.size();

        int i = 0, j = 0;

        Map<Pair<Integer, Integer>, Double> simplifiedPayables = Maps.newHashMap();
//        List<Double> payableAmounts = Lists.newArrayList(sortedPayables.values());
//        List<Integer> payableAmountKeys = Lists.newArrayList(sortedPayables.keySet());
//        List<Double> receivableAmounts = Lists.newArrayList(sortedReceivales.values());
//        List<Integer> receivableAmountKeys = Lists.newArrayList(sortedReceivales.keySet());

        while (i < lenPayables && j < lenReceivables) {
            Double currPayable = payables.get(i).getValue();
            Double currReceivable = receivables.get(j).getValue();

            //case 1 - match
            if (Math.abs(currReceivable - currPayable) < .01) {
                simplifiedPayables.put(new Pair<>(payables.get(i).getKey(), receivables.get(j).getKey()), currPayable);
                i++;
                j++;

            }
            //case 2
            else if (currReceivable > currPayable) {
                simplifiedPayables.put(new Pair<>(payables.get(i).getKey(), receivables.get(j).getKey()), currPayable);
                //payable is exhausted but receivable is yet not exhausted
                i++;
                //update remaining receivable
                receivables.set(j, new Pair<>(receivables.get(j).getKey(), currReceivable - currPayable));
            }
            //case 3
            else if (currReceivable < currPayable) {
                simplifiedPayables.put(new Pair<>(payables.get(i).getKey(), receivables.get(j).getKey()), currReceivable);
                //receivable is exhausted but payable is yet not exhausted
                j++;
                //update remaining payable
                payables.set(i, new Pair<>(payables.get(i).getKey(), currPayable - currReceivable));
            }

            //TODO: will it be possible that we run out of payable or receivables??

            logger.info(" simplifiedPayables = {}", simplifiedPayables);

        }


    }

    public LinkedHashMap<Integer, Double> getSortedMapByValue(Map<Integer, Double> hm) {
        List<Map.Entry<Integer, Double>> list =
                new LinkedList<>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Integer, Double>>() {
            public int compare(Map.Entry<Integer, Double> o1,
                               Map.Entry<Integer, Double> o2) {
                if (Math.abs(o1.getValue() - o2.getValue()) > .01) {
                    return -1;
                } else if (Math.abs(o1.getValue() - o2.getValue()) < .01) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });

        // put data from sorted list to hashmap
        LinkedHashMap<Integer, Double> temp = new LinkedHashMap<Integer, Double>();
        for (Map.Entry<Integer, Double> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

}

package com.demo.splitwise.dao;

import com.demo.splitwise.model.User;
import com.demo.splitwise.model.UserGroup;
import org.springframework.data.repository.CrudRepository;

public interface UserGroupRepo extends CrudRepository<UserGroup, Integer> {
}

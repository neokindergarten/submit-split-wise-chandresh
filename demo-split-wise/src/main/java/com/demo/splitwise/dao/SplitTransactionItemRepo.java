package com.demo.splitwise.dao;

import com.demo.splitwise.model.SplitTransactionItem;
import com.demo.splitwise.model.SplitTransactionItemId;
import org.springframework.data.repository.CrudRepository;

public interface SplitTransactionItemRepo extends CrudRepository<SplitTransactionItem, SplitTransactionItemId> {
}

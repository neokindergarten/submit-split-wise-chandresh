package com.demo.splitwise.dao;

import com.demo.splitwise.model.SplitTransaction;
import com.demo.splitwise.model.UserGroup;
import org.springframework.data.repository.CrudRepository;

public interface SplitTransactionRepo extends CrudRepository<SplitTransaction, Integer> {
}

package com.demo.splitwise.dao;

import com.demo.splitwise.model.Balance;
import com.demo.splitwise.model.Expense;
import com.demo.splitwise.model.GroupBalance;
import com.demo.splitwise.model.SplitTransaction;
import com.demo.splitwise.service.ExpenseService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class ExpenseDao implements InitializingBean {

    Logger logger = LoggerFactory.getLogger(ExpenseService.class);

    @Autowired
    private Environment env;


    @Autowired
    JdbcTemplate jdbcTemplate;


    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Override
    public void afterPropertiesSet() throws Exception {
        namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Expense getExpense(Integer billId) {

        String sql = env.getProperty("SELECT_EXPENSE");
        logger.info("expense query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("billId", billId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<Expense>() {
            Map<Integer, Expense> result = Maps.newHashMap();

            @Override
            public Expense extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                while (resultSet.next()) {

                    Integer id = resultSet.getInt("bill_id");
                    Double billAmt = resultSet.getDouble("bill_amount");
                    Integer groupId = resultSet.getInt("group_id");
                    String type = resultSet.getString("split_type");
                    Integer paidBy = resultSet.getInt("paid_by");

                    result.putIfAbsent(id, new Expense());
                    result.get(id).setBillId(id);
                    result.get(id).setTotalBillAmount(billAmt);
                    result.get(id).setGroupId(groupId);
                    result.get(id).setType(type);
                    result.get(id).setPaidBy(paidBy);

                    Integer user = resultSet.getInt("user");
                    Double userAmount = resultSet.getDouble("user_amount");

                    result.get(id).addSplit(user, userAmount);

                }

                return result.get(billId);
            }
        });


    }

    public List<SplitTransaction> getBillsPaidByUser(Integer userId) {

        String sql = env.getProperty("SELECT_TRANSACTION_BY_USER");
        logger.info("query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("userId", userId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<List<SplitTransaction>>() {
            List<SplitTransaction> result = Lists.newArrayList();

            @Override
            public List<SplitTransaction> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                while (resultSet.next()) {

                    Integer id = resultSet.getInt("bill_id");
                    Double billAmt = resultSet.getDouble("bill_amount");
                    Integer groupId = resultSet.getInt("group_id");
                    String type = resultSet.getString("split_type");
                    Integer paidBy = resultSet.getInt("paid_by");

                    SplitTransaction txn = new SplitTransaction();
                    txn.setSplitType(type);
                    txn.setId(id);
                    txn.setGroupId(groupId);
                    txn.setUserId(userId);
                    txn.setAmount(billAmt);

                    result.add(txn);

                }

                return result;
            }
        });

    }

    public Balance getUserBalance(Integer userId) {

        Balance bal = new Balance();
        bal.setUserId(userId);
        bal.setBillsPaidByUser(getBillsPaidByUser(userId));
        bal.setAmountOwedToUser(getAmountOwedToUser(userId));
        bal.setAmountOwedByUser(getAmountOwedByUser(userId));

        bal.setTotalPaid(0.);
        bal.setTotalOutstanding(0.);
        bal.setTotalReceivables(0.);

        if (Objects.nonNull(bal.getBillsPaidByUser()) && bal.getBillsPaidByUser().size() > 0) {
            bal.getBillsPaidByUser().stream().forEach(x -> bal.setTotalPaid(bal.getTotalPaid() + x.getAmount()));

        }

        if (Objects.nonNull(bal.getAmountOwedToUser()) && bal.getAmountOwedToUser().size() > 0) {
            bal.getAmountOwedToUser().forEach((k, v) -> {
                bal.setTotalReceivables(bal.getTotalReceivables() + v);
            });

        }

        if (Objects.nonNull(bal.getAmountOwedByUser()) && bal.getAmountOwedByUser().size() > 0) {
            bal.getAmountOwedByUser().forEach((k, v) -> {
                bal.setTotalOutstanding(bal.getTotalOutstanding() + v);
            });
        }

        logger.info("user {} balance {}", userId, bal);
        return bal;

    }

    private Map<Integer, Double> getAmountOwedToUser(Integer userId) {
        String sql = env.getProperty("SELECT_AMT_OWED_TO_USER");
        logger.info("query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("userId", userId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<Map<Integer, Double>>() {
            Map<Integer, Double> result = Maps.newHashMap();

            @Override
            public Map<Integer, Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {
                    //TO_USER, FROM_USER, TOTAL
                    Integer toUser = rs.getInt("TO_USER");
                    Integer fromUser = rs.getInt("FROM_USER");
                    Double amount = rs.getDouble("TOTAL");
                    result.put(fromUser, amount);

                }

                return result;
            }
        });


    }

    private Map<Integer, Double> getAmountOwedByUser(Integer userId) {
        String sql = env.getProperty("SELECT_AMT_OWED_BY_USER");
        logger.info("query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("userId", userId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<Map<Integer, Double>>() {
            Map<Integer, Double> result = Maps.newHashMap();

            @Override
            public Map<Integer, Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {
                    //TO_USER, FROM_USER, TOTAL
                    Integer toUser = rs.getInt("TO_USER");
                    Integer fromUser = rs.getInt("FROM_USER");
                    Double amount = rs.getDouble("TOTAL");
                    result.put(toUser, amount);

                }

                return result;
            }
        });


    }


    public GroupBalance getGroupBalance(Integer groupId) {

        GroupBalance bal = new GroupBalance();
        bal.setGroupId(groupId);
        bal.setTransactionsInGroup(getTransactionsForGroup(groupId));
        bal.setTotalExpense(0.);
        if (Objects.nonNull(bal.getTransactionsInGroup()) && bal.getTransactionsInGroup().size() > 0) {
            bal.getTransactionsInGroup().stream().forEach(x -> bal.setTotalExpense(bal.getTotalExpense() + x.getAmount()));

        }
        bal.setPayables(getGroupPayables(groupId));
        bal.setSimplifiedPayables(getSimpliedGroupPayables(bal));

        logger.info("group {} balance {}", groupId, bal);
        return bal;

    }

    public Map<Pair<Integer, Integer>, Double> getSimpliedGroupPayables(GroupBalance bal) {
        List<Pair<Integer,Double>> sortedPayable = Lists.newArrayList();
        List<Pair<Integer,Double>> sortedReceivable = Lists.newArrayList();
        bal.getPayables().forEach((K,V)->{

            sortedPayable.add(new Pair<>(null,null));

        });

        return null;
    }

    private Map<Pair<Integer, Integer>, Double> getGroupPayables(Integer groupId) {
        String sql = env.getProperty("SELECT_AMT_OWED_IN_GROUP");
        logger.info("query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("groupId", groupId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<Map<Pair<Integer, Integer>, Double>>() {
            Map<Pair<Integer, Integer>, Double> result = Maps.newHashMap();

            @Override
            public Map<Pair<Integer, Integer>, Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {
                    //TO_USER, FROM_USER, TOTAL
                    Integer toUser = rs.getInt("TO_USER");
                    Integer fromUser = rs.getInt("FROM_USER");
                    Double amount = rs.getDouble("TOTAL");
                    Pair<Integer, Integer> pair = new Pair<>(fromUser, toUser);

                    result.put(pair, amount);

                }

                return result;
            }
        });


    }

    private List<SplitTransaction> getTransactionsForGroup(Integer groupId) {

        String sql = env.getProperty("SELECT_TRANSACTION_BY_GROUP");
        logger.info("query {}", sql);

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("groupId", groupId);

        return namedJdbcTemplate.query(sql, map, new ResultSetExtractor<List<SplitTransaction>>() {
            List<SplitTransaction> result = Lists.newArrayList();

            @Override
            public List<SplitTransaction> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                while (resultSet.next()) {

                    Integer id = resultSet.getInt("bill_id");
                    Double billAmt = resultSet.getDouble("bill_amount");
                    Integer groupId = resultSet.getInt("group_id");
                    String type = resultSet.getString("split_type");
                    Integer paidBy = resultSet.getInt("paid_by");

                    SplitTransaction txn = new SplitTransaction();
                    txn.setSplitType(type);
                    txn.setId(id);
                    txn.setGroupId(groupId);
                    txn.setUserId(paidBy);
                    txn.setAmount(billAmt);

                    result.add(txn);

                }

                return result;
            }
        });

    }
}

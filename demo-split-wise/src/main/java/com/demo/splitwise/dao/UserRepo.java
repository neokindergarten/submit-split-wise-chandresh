package com.demo.splitwise.dao;

import com.demo.splitwise.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, Integer> {
}

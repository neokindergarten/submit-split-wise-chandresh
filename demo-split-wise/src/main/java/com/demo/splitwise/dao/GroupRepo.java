package com.demo.splitwise.dao;

import com.demo.splitwise.model.Group;
import com.demo.splitwise.model.User;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepo extends CrudRepository<Group, Integer> {
}

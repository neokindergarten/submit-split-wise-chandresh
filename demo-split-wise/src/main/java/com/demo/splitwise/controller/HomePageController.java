package com.demo.splitwise.controller;

import com.demo.splitwise.dao.GroupRepo;
import com.demo.splitwise.dao.SplitTransactionRepo;
import com.demo.splitwise.dao.UserGroupRepo;
import com.demo.splitwise.dao.UserRepo;
import com.demo.splitwise.model.*;
import com.demo.splitwise.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class HomePageController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    GroupRepo groupRepo;

    @Autowired
    UserGroupRepo userGroupRepo;

    @Autowired
    SplitTransactionRepo splitTransactionRepo;

    @Autowired
    ExpenseService expenseService;

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    @ResponseBody
    public List<Group> getGroups(){
        Iterator<Group> it = groupRepo.findAll().iterator();
        List<Group> groups = new ArrayList<>();

        while(it.hasNext()){
            groups.add(it.next());
        }
        return groups;
    }

    @RequestMapping(value = "/group/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Group getGroup(@PathVariable(name = "id")Integer id){
        System.out.println("Inside getGroup Begins");
        System.out.println("id"+ id);
        System.out.println(groupRepo.findById(Integer.valueOf(id)).get());
        return groupRepo.findById(Integer.valueOf(id)).get();
    }

    @RequestMapping(value = "/addGroup", method = RequestMethod.POST)
    public ResponseEntity addGroup(@RequestBody Group group) {
        System.out.println("Inside addGroup Begins");
        System.out.println(group);
        groupRepo.save(group);
        System.out.println("Inside addGroup Ends");
        return new ResponseEntity("Add Group Success", HttpStatus.OK);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers(){
        Iterator<User> it = userRepo.findAll().iterator();
        List<User> users = new ArrayList<>();

        while(it.hasNext()){
            users.add(it.next());
        }
        return users;
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable(name = "id")Integer id){
        System.out.println("Inside getUser Begins");
        System.out.println("id"+ id);
        System.out.println(userRepo.findById(Integer.valueOf(id)).get());
        return userRepo.findById(id).get();
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseEntity addUser(@RequestBody User user) {
        System.out.println("Inside addUser Begins");
        System.out.println(user);
        userRepo.save(user);
        System.out.println("Inside addUser Ends");
        return new ResponseEntity("Add User Success", HttpStatus.OK);
    }

    @RequestMapping(value = "/userToGroup", method = RequestMethod.GET)
    @ResponseBody
    public List<UserGroup> getUserGroups(){
        Iterator<UserGroup> it = userGroupRepo.findAll().iterator();
        List<UserGroup> userGroups = new ArrayList<>();

        while(it.hasNext()){
            userGroups.add(it.next());
        }
        return userGroups;
    }

    @RequestMapping(value = "/addUserToGroup", method = RequestMethod.POST)
    public ResponseEntity addUserToGroup(@RequestBody UserGroup userGroup) {
        System.out.println("Inside addUserToGroup Begins");
        System.out.println(userGroup);
        userGroupRepo.save(userGroup);
        System.out.println("Inside addUserToGroup Ends");
        return new ResponseEntity("Add User To Group Success", HttpStatus.OK);
    }

    @RequestMapping(value = "/addExpense", method = RequestMethod.POST)
    public ResponseEntity addExpense(@RequestBody Expense expense) {
        System.out.println("Inside addExpense Begins");
        System.out.println(expense);
        expenseService.addExpense(expense);
        System.out.println("Inside addExpense Ends");
        return new ResponseEntity("Add Expense Success", HttpStatus.OK);
    }


    @RequestMapping(value = "/expense/{billId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getExpense(@PathVariable(name = "billId") Integer billId) {
        System.out.println("Inside getExpense Begins");
        Expense expense = expenseService.getExpense(billId);
        System.out.println("Inside getExpense Ends");
        return new ResponseEntity(expense, HttpStatus.OK);

    }

    @RequestMapping(value = "/balance/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getBalance(@PathVariable(name = "userId") Integer userId) {
        System.out.println("Inside getBalance Begins");
        Balance bal = expenseService.getUserBalance(userId);
        System.out.println("Inside getBalance Ends");
        return new ResponseEntity(bal, HttpStatus.OK);

    }


    @RequestMapping(value = "/groupbalance/{groupId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getGroupBalance(@PathVariable(name =  "groupId") Integer groupId) {
        System.out.println("Inside getGroupBalance Begins");
        GroupBalance bal = expenseService.getGroupBalance(groupId);
        System.out.println("Inside getGroupBalance Ends");
        return new ResponseEntity(bal, HttpStatus.OK);

    }
}

package com.demo.splitwise.model;

import javafx.util.Pair;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GroupBalance {

    private Integer groupId;

    private Double totalExpense;

    private List<SplitTransaction> transactionsInGroup;
    private Map<Pair<Integer, Integer>, Double> payables;
    private Map<Pair<Integer, Integer>, Double> simplifiedPayables;


    public GroupBalance() {
    }

    public Double getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(Double totalExpense) {
        this.totalExpense = totalExpense;
    }

    public Map<Pair<Integer, Integer>, Double> getPayables() {
        return payables;
    }

    public void setPayables(Map<Pair<Integer, Integer>, Double> payables) {
        this.payables = payables;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public List<SplitTransaction> getTransactionsInGroup() {
        return transactionsInGroup;
    }

    public void setTransactionsInGroup(List<SplitTransaction> transactionsInGroup) {
        this.transactionsInGroup = transactionsInGroup;
    }

    public Map<Pair<Integer, Integer>, Double> getSimplifiedPayables() {
        return simplifiedPayables;
    }

    public void setSimplifiedPayables(Map<Pair<Integer, Integer>, Double> simplifiedPayables) {
        this.simplifiedPayables = simplifiedPayables;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupBalance that = (GroupBalance) o;
        return Objects.equals(groupId, that.groupId) &&
                Objects.equals(totalExpense, that.totalExpense) &&
                Objects.equals(transactionsInGroup, that.transactionsInGroup) &&
                Objects.equals(payables, that.payables) &&
                Objects.equals(simplifiedPayables, that.simplifiedPayables);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, totalExpense, transactionsInGroup, payables, simplifiedPayables);
    }

    @Override
    public String toString() {
        return "GroupBalance{" +
                "groupId=" + groupId +
                ", totalExpense=" + totalExpense +
                ", transactionsInGroup=" + transactionsInGroup +
                ", payables=" + payables +
                ", simplifiedPayables=" + simplifiedPayables +
                '}';
    }
}

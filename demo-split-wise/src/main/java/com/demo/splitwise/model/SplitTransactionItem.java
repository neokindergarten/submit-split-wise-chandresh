package com.demo.splitwise.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="SPLIT_TRANSACTION_ITEM")
public class SplitTransactionItem {

    @EmbeddedId
    private SplitTransactionItemId id;

    private Double amount;

    public SplitTransactionItem(){

    }

    public SplitTransactionItemId getId() {
        return id;
    }

    public void setId(SplitTransactionItemId id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SplitTransactionItem that = (SplitTransactionItem) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, amount);
    }

    @Override
    public String toString() {
        return "SplitTransactionItem{" +
                "id=" + id +
                ", amount=" + amount +
                '}';
    }
}

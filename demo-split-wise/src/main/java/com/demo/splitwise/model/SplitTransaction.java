package com.demo.splitwise.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="SPLIT_TRANSACTION")
public class SplitTransaction {

    @Id
    @GeneratedValue
    private Integer id;
    private Double amount;
    private Integer groupId;
    private Integer userId;
    private String splitType;

    public SplitTransaction(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSplitType() {
        return splitType;
    }

    public void setSplitType(String splitType) {
        this.splitType = splitType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SplitTransaction that = (SplitTransaction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(groupId, that.groupId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(splitType, that.splitType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, amount, groupId, userId, splitType);
    }

    @Override
    public String toString() {
        return "SplitTransaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", groupId=" + groupId +
                ", userId=" + userId +
                ", splitType='" + splitType + '\'' +
                '}';
    }
}

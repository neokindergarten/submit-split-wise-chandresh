package com.demo.splitwise.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SplitTransactionItemId implements Serializable{

    private Integer billId;
    private Integer userId;

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public SplitTransactionItemId(){

    }

    @Override
    public String toString() {
        return "SplitTransactionItemId{" +
                "billId=" + billId +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SplitTransactionItemId that = (SplitTransactionItemId) o;
        return Objects.equals(billId, that.billId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(billId, userId);
    }
}

package com.demo.splitwise.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SPLIT_GROUP")
public class Group {
    @Id
    @GeneratedValue
    private Integer id;
    private String groupName;
    private String createdBy;

    public Group() {
    }

    public Group(Integer id, String groupName, String createdBy) {
        this.id = id;
        this.groupName = groupName;
        this.createdBy = createdBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}

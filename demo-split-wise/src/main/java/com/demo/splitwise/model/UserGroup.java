package com.demo.splitwise.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "SPLIT_USER_GROUP")
public class UserGroup {

    @EmbeddedId
    private UserGroupId userId;

    public UserGroup() {

    }

    public UserGroup(Integer uid, Integer gid) {
        this.userId = new UserGroupId(uid, gid);
    }

    public UserGroupId getUserId() {
        return userId;
    }

    public void setUserId(UserGroupId userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserGroup userGroup = (UserGroup) o;
        return Objects.equals(userId, userGroup.userId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), userId);
    }
}

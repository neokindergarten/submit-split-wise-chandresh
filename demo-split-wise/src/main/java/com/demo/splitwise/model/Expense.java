package com.demo.splitwise.model;

import com.google.common.collect.Maps;

import java.util.Map;

public class Expense {

    private Integer groupId;
    private Integer billId;
    private Integer paidBy;
    private Double totalBillAmount;
    private String type;
    private String splitsString;

    private Map<Integer, Double> splits;


    public Expense() {
        splits = Maps.newHashMap();
    }
    public Expense(Integer billId,
                   Integer groupId,
                   Integer paidBy,
                   Double totalBillAmount,
                   String type,
                   String splitsString) {

        this.groupId = groupId;
        this.billId = billId;
        this.paidBy = paidBy;
        this.totalBillAmount = totalBillAmount;
        this.type = type;
        this.splitsString = splitsString;
        splits = Maps.newHashMap();
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(Integer paidBy) {
        this.paidBy = paidBy;
    }

    public Double getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(Double totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSplitsString() {
        return splitsString;
    }

    public void setSplitsString(String splitsString) {
        this.splitsString = splitsString;
    }

    public Map<Integer, Double> getSplits() {
        return splits;
    }

    public void setSplits(Map<Integer, Double> splits) {
        this.splits = splits;
    }

    public void addSplit(Integer uid, Double amt) {
        splits.put(uid, amt);

    }


    @Override
    public String toString() {
        return "Expense{" +
                "groupId=" + groupId +
                ", billId=" + billId +
                ", paidBy=" + paidBy +
                ", totalBillAmount=" + totalBillAmount +
                ", type='" + type + '\'' +
                ", splitsString='" + splitsString + '\'' +
                '}';
    }
}


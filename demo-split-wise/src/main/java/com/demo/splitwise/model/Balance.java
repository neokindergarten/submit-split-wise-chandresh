package com.demo.splitwise.model;

import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Balance {

    private Integer userId;

    private Double totalPaid;
    private Double totalReceivables;
    private Double totalOutstanding;

    private List<SplitTransaction> billsPaidByUser;
    private Map<Integer, Double> amountOwedToUser;
    private Map<Integer, Double> amountOwedByUser;

    public Balance() {
    }

    public Balance(Integer userId) {
        this.userId = userId;
        amountOwedToUser = Maps.newHashMap();
        amountOwedByUser = Maps.newHashMap();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Balance balance = (Balance) o;
        return Objects.equals(userId, balance.userId) &&
                Objects.equals(amountOwedToUser, balance.amountOwedToUser) &&
                Objects.equals(amountOwedByUser, balance.amountOwedByUser);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, amountOwedToUser, amountOwedByUser);
    }

    @Override
    public String toString() {
        return "Balance{" +
                "userId=" + userId +
                ", amountOwedToUser=" + amountOwedToUser +
                ", amountOwedByUser=" + amountOwedByUser +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Map<Integer, Double> getAmountOwedToUser() {
        return amountOwedToUser;
    }

    public void setAmountOwedToUser(Map<Integer, Double> amountOwedToUser) {
        this.amountOwedToUser = amountOwedToUser;
    }

    public Map<Integer, Double> getAmountOwedByUser() {
        return amountOwedByUser;
    }

    public void setAmountOwedByUser(Map<Integer, Double> amountOwedByUser) {
        this.amountOwedByUser = amountOwedByUser;
    }

    public List<SplitTransaction> getBillsPaidByUser() {
        return billsPaidByUser;
    }

    public void setBillsPaidByUser(List<SplitTransaction> billsPaidByUser) {
        this.billsPaidByUser = billsPaidByUser;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;

        if (Objects.nonNull(billsPaidByUser) && billsPaidByUser.size() > 1) {
            this.totalPaid = 0.;
            billsPaidByUser.stream().forEach(x -> this.totalPaid = this.totalPaid + x.getAmount());
        }
    }

    public Double getTotalReceivables() {
        return totalReceivables;
    }

    public void setTotalReceivables(Double totalReceivables) {

        this.totalReceivables = totalReceivables;

        if (Objects.nonNull(amountOwedToUser) && amountOwedToUser.size() > 1) {
            this.totalReceivables = 0.;
            amountOwedToUser.forEach((k, v) -> {
                this.totalReceivables = this.totalReceivables + v;
            });

        }
    }

    public Double getTotalOutstanding() {
        return totalOutstanding;
    }

    public void setTotalOutstanding(Double totalOutstanding) {
        this.totalOutstanding = totalOutstanding;

        if (Objects.nonNull(amountOwedByUser) && amountOwedByUser.size() > 1) {
            this.totalOutstanding = 0.;
            amountOwedByUser.forEach((k, v) -> {
                this.totalOutstanding = this.totalOutstanding + v;
            });
            System.out.println("called after summation=>" + totalOutstanding);
        }
    }
}

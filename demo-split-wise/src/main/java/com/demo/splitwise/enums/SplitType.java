package com.demo.splitwise.enums;

public enum SplitType {

    FIXED,
    PERCENT,
}

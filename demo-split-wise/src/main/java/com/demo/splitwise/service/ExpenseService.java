package com.demo.splitwise.service;

import com.demo.splitwise.dao.ExpenseDao;
import com.demo.splitwise.dao.SplitTransactionItemRepo;
import com.demo.splitwise.dao.SplitTransactionRepo;
import com.demo.splitwise.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class ExpenseService {

    org.slf4j.Logger logger = LoggerFactory.getLogger(ExpenseService.class);

    @Autowired
    private SplitTransactionRepo splitTransactionRepo;

    @Autowired
    private SplitTransactionItemRepo splitTransactionItemRepo;

    @Autowired
    private ExpenseDao expenseDao;


    public void addExpense(Expense expense) {

        Map<String, String> splits = populateMapFromString(expense);

        logger.info("processing expense {}", expense);

        SplitTransaction txn = new SplitTransaction();
        txn.setAmount(expense.getTotalBillAmount());
        txn.setGroupId(expense.getGroupId());
        txn.setUserId(expense.getPaidBy());
        txn.setSplitType(expense.getType());

        SplitTransaction savedTxn = splitTransactionRepo.save(txn);

        logger.info("saved  SplitTransaction {}", savedTxn);

        splits.forEach((String user, String amount) -> {
            SplitTransactionItem item = new SplitTransactionItem();
            item.setId(new SplitTransactionItemId());
            item.getId().setBillId(savedTxn.getId());
            item.getId().setUserId(Integer.valueOf(user));
            item.setAmount(Double.valueOf(amount));
            SplitTransactionItem savedItem = splitTransactionItemRepo.save(item);
            logger.info("saved  SplitTransactionItem {}", savedItem);
        });
    }

    public Expense getExpense(Integer billId) {

        Optional<SplitTransaction> dbTxn = splitTransactionRepo.findById(billId);
        if (dbTxn.isPresent()) {

            SplitTransaction txn = dbTxn.get();

            Expense expense = new Expense();
            expense.setBillId(txn.getId());
            expense.setGroupId(txn.getGroupId());
            expense.setPaidBy(txn.getUserId());
            expense.setTotalBillAmount(txn.getAmount());

            return expense;
        }
        return null;
    }

    public Map populateMapFromString(Expense expense) {

        String jsonString = expense.getSplitsString();
        Map splits = new HashMap();
        ObjectMapper mapper = new ObjectMapper();

        try {
            logger.info("jsonString {}", jsonString);
            splits = mapper.readValue(jsonString, Map.class);
            splits = prepareMapWithSplitAmounts(splits, expense.getType(), expense.getTotalBillAmount());
            logger.info("processed splits {}", splits.toString());
            return splits;

        } catch (IOException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        return splits;
    }

    public Balance getUserBalance(Integer userId) {

        return expenseDao.getUserBalance(userId);
    }

    public Map prepareMapWithSplitAmounts(Map<String, String> splits, String splitType, Double totalBillValue) {
        Map<String, String> finalSplits = new HashMap();
        splits.forEach((String user, String breakupValue) -> {
            if (splitType.equals("equal")) {
                finalSplits.put(user, String.valueOf(totalBillValue / splits.size()));
            } else if (splitType.equals("value")) {
                finalSplits.put(user, breakupValue);
            } else if (splitType.equals("percentage")) {
                finalSplits.put(user, String.valueOf(totalBillValue * (Double.valueOf(breakupValue) / 100)));
            }

        });
        logger.info("prepareMapWithSplitAmounts {}", finalSplits);
        return finalSplits;
    }

    public GroupBalance getGroupBalance(Integer groupId) {

        return expenseDao.getGroupBalance(groupId);
    }
}
